use <../misc/helper.scad>

curvature = 250;
curve_length = 15; //degrees
straight_piece_length = 30;
diameter = 20;

stop_thickness=3;
stop_small_diameter=diameter+1;
stop_extra_length=5;

$fa=1;

module handle() {
    rotate([0, 0, 90])
    // Rotate the actual handle over the circle, and move back.
    translate([-curvature, 0, 0])
    rotate([0, 0, -curve_length])
    translate([curvature, -straight_piece_length, 0])
    handle_impl();
}

module handle_stop() {
    rotate([90, 0, 0])
    hull() {
        torus_part(stop_small_diameter/2, stop_thickness/2, 360);
        translate([stop_extra_length, 0, 0])
        torus_part(stop_small_diameter/2, stop_thickness/2, 360);
    }
}

module bumps() {
    // From pictures, it seems like the yoke's handle has four bumps, evenly spaced over the handle.
    
    bump_dh=1;
    bump_r=15/2;
    bump_h=3;
    bump_slope=1.5;
    
    module bump() {
        rotate([90, 0, 0])
        translate([-bump_dh-diameter/2+bump_r, 0, 0])
        hull() {
            torus_part(R=bump_r-bump_h, r=bump_h, angle=360);
            cylinder(h=2*bump_slope*bump_r, r=bump_h, center=true);
        }
    }
    
    handle_inner_length=straight_piece_length + curvature*curve_length/180*PI;
    for(bump_i=[0:3]) {
        bump_position = handle_inner_length/5*(bump_i+1);
        if(bump_position <= straight_piece_length) {
            translate([0, bump_position, 0])
            bump();
        } else {
            curve_travel = (bump_position - straight_piece_length)/curvature*180/PI;
            translate([-curvature, straight_piece_length, 0])
            rotate([0, 0, curve_travel])
            translate([curvature, 0, 0])
            bump();
        }
    }
}

module handle_impl() {
    translate([0, straight_piece_length, 0])
        torus_part(curvature, diameter/2, curve_length, true);

    rotate([-90, 0, 0])
        cylinder(r=diameter/2, h=straight_piece_length);
    
    handle_stop();
    bumps();
}

handle_impl($fa=0.5, $fs=0.5);