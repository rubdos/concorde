include <yoke_config.scad>
use <bar.scad>
use <handle.scad>
use <fork.scad>

$fs=1;
$fa=1;

module right() {
    right_bar();
    translate([0, 0, bar_length-bar_diameter/2])
    rotate([90, -10, 0])
    handle();
}

module left() {
    left_bar();
    translate([0, 0, bar_length-bar_diameter/2])
    rotate([90, -10, 180])
    handle();
}

translate([fork_x_displacement()/2, fork_depth_displacement(), fork_y_displacement()])
rotate([-fork_pitch, fork_angle/2, 0])
right();

translate([-fork_x_displacement()/2, fork_depth_displacement(), fork_y_displacement()])
rotate([-fork_pitch, -fork_angle/2, 0])
left();

fork();