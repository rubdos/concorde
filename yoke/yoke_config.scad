// Configuration is seen from the FO's perspective.

bar_length=130;
bar_diameter=25;

fork_angle=75;
fork_curvature=20;
fork_side_curvature=30;
fork_side_angle=50;
fork_side_offset=15;
fork_pitch=20;
fork_pitch_curvature=20;

// 3D printing options
for_3d_printer = true;
wall = 3;
epsilon = 0.05;
layer_height = 1; // set to layer height of printer
