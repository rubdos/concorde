include <yoke_config.scad>
use <../misc/helper.scad>

torus_angle=180-fork_angle;

module fork_pitch(hollow=false) {
    rotate([90, 0, -90])
    if(hollow) {
    torus_part(fork_pitch_curvature, bar_diameter/2, fork_pitch, true, wall);
    } else {
    torus_part(fork_pitch_curvature, bar_diameter/2, fork_pitch, true);
    }
}

module fork(hollow=false) {
    torus_angle=180-fork_angle;
    module upper(inner=false) {
        translate([0, 0, cos(torus_angle/2)*(fork_curvature+bar_diameter/2)])
        rotate([90, -fork_angle/2 + 180, 0])
        if(inner)
        torus_part(fork_curvature+bar_diameter/2, bar_diameter/2-wall, torus_angle+4*epsilon);
        else {
            if(for_3d_printer) {
            torus_part(fork_curvature+bar_diameter/2, bar_diameter/2, torus_angle+2*epsilon, false, wall);
            } else {
                torus_part(fork_curvature+bar_diameter/2, bar_diameter/2, torus_angle+2*epsilon);
            }
        }
    }
    
    module side(hollow=false) {
        translate([-internal_fork_displacement()/2, 0, 0])
        rotate([-90, torus_angle/2-90, 0]) {
            rotate([-90, 0, 0])
            if(hollow)
            translate([0, 0, -epsilon])
            rotate([0, 0, -90]) cylinder(r=bar_diameter/2-wall, h=fork_side_offset+epsilon*2);
            else if(for_3d_printer)
            {
                difference() {
                    translate([0, 0, -epsilon])rotate([0, 0, -90])cylinder(r=bar_diameter/2, h=fork_side_offset+2*epsilon);
                    translate([0, 0, -2*epsilon])
                    rotate([0, 0, -90])cylinder(r=bar_diameter/2-wall, h=fork_side_offset+4*epsilon);
                }
            }
            else
            rotate([0, 0, -90]) cylinder(r=bar_diameter/2, h=fork_side_offset);
            translate([0, fork_side_offset, 0])
                if(hollow)
                torus_part(fork_side_curvature + bar_diameter/2,
                    bar_diameter/2-wall,
                    fork_side_angle, true);
                else if(for_3d_printer)
                torus_part(fork_side_curvature + bar_diameter/2,
                    bar_diameter/2,
                    fork_side_angle, true, wall);
                else
                torus_part(fork_side_curvature + bar_diameter/2,
                    bar_diameter/2,
                    fork_side_angle, true);
        }
    }
    
    module filler(epsilon=0) {
        difference() {
            hull() {
                circle(r=fork_curvature+bar_diameter/2+epsilon);
                translate([0, -bar_diameter])
                circle(r=fork_curvature+epsilon);
            }
            translate([0, fork_curvature + cos(torus_angle/2)*(fork_curvature+bar_diameter/2)])
                circle(r=fork_curvature+bar_diameter/2-epsilon);
            //////
            translate([
                -internal_fork_displacement()/2,
                fork_curvature+epsilon])
            rotate(-90-torus_angle/2)
            square([bar_diameter, fork_side_offset]);
            
            translate([
                internal_fork_displacement()/2,
                fork_curvature+epsilon])
            mirror([1, 0])
            rotate(-90-torus_angle/2)
            square([bar_diameter, fork_side_offset]);
            //////
            translate([
                -internal_fork_displacement()/2,
                fork_curvature])
            rotate(-90-torus_angle/2)
            translate([fork_side_curvature+bar_diameter/2, fork_side_offset])
            circle(r=fork_side_curvature+bar_diameter/2-epsilon);
            
            translate([
                internal_fork_displacement()/2,
                fork_curvature])
            rotate(90+torus_angle/2)
            translate([-fork_side_curvature-bar_diameter/2, fork_side_offset])
            circle(r=fork_side_curvature+bar_diameter/2-epsilon);
        }
    }
    
    module cutoff() {
        translate([0, -bar_diameter/2-wall/2, cos(torus_angle/2)*(fork_curvature+bar_diameter/2)])
        rotate([0, 180-torus_angle/2, 0])
        translate([fork_side_offset, 0, fork_curvature + bar_diameter + fork_side_curvature])
        rotate([0, torus_angle/2+fork_angle/2-fork_side_angle, 0])
        translate([fork_side_curvature, 0, 0])
        rotate([0, -fork_angle/2+fork_side_angle, 0])
        cube([fork_x_displacement(), bar_diameter+wall, 100]);
    }
    
    difference() {
        union() {
            upper(false);
            
            translate([internal_fork_displacement()/2, 0, 0])
            rotate([0, fork_angle/2, 0])
            fork_pitch(hollow=for_3d_printer);
            
            translate([-internal_fork_displacement()/2, 0, 0])
            rotate([0, -fork_angle/2, 0])
            fork_pitch(hollow=for_3d_printer);
            
            union() {
            side();
            rotate([0, 0, 180])
            side();
            translate([0, bar_diameter/2+epsilon, -fork_curvature])
            rotate([90, 0, 0])
            linear_extrude(height=wall)
            filler();
            translate([0, -bar_diameter/2-epsilon+wall, -fork_curvature])
            rotate([90, 0, 0])
            linear_extrude(height=wall)
            filler();
            }
        }
        cutoff();
        if(for_3d_printer)
        union() {
            %upper(true);
            side(hollow=true);
            rotate([0, 0, 180])
            side(hollow=true);
            translate([0, bar_diameter/2-wall, -fork_curvature])
            rotate([90, 0, 0])
            linear_extrude(height=bar_diameter-2*wall)
            %filler(epsilon*2);
        }
    }
}

function internal_fork_displacement() = sin(torus_angle/2)*(fork_curvature+bar_diameter/2)*2;

function pitch_displacement() = sin(fork_pitch)*fork_pitch_curvature;

function fork_x_displacement() = 2*sin(fork_angle/2)*pitch_displacement() + internal_fork_displacement();
function fork_y_displacement() = cos(fork_angle/2)*pitch_displacement();
function fork_depth_displacement() = fork_pitch_curvature*(1-cos(fork_pitch));

fork($fa=1, $fs=1.25);
