use <scad-utils/morphology.scad>
include <yoke_config.scad>
use <handle.scad>

right_bar_upper_length=50;
right_bar_transition_length=40;

left_bar_upper_length=2*bar_length/3;
left_bar_transition_length=bar_length/6;

fillet=3;

module profile(t /* 0 -> 1*/, d) {
    if (t == 0) {
        circle(r=d/2);
    } else {
        r=d/2 - t*(d/2 - fillet);
        rounding(r) square([d, d], center=true);
    }
}

// TODO: find a decent way to implement this.
module transition(h, d=bar_diameter) {
    layer_count = floor(h/layer_height);
    for(i=[0:layer_count-1]) {
        dh = h/layer_count;
        t = i/(layer_count-1);
        translate([0, 0, dh*i]) {
            linear_extrude(height=dh)
                profile(t, d);
        }
    }
}

module upper_part(l, d=bar_diameter) {
    translate([-(d-2*fillet)/2, -(d-2*fillet)/2, 0])
    minkowski() {
        cube([d-2*fillet, d-2*fillet, l]);
        cylinder(r=fillet, h=1);
    }
}

module bar(
    transition_length,
    upper_length
) {
    difference() {
        union() {
            lower_length = bar_length - transition_length - upper_length;
            cylinder(h=lower_length, r=bar_diameter/2);
            
            translate([0, 0, lower_length])
            transition(transition_length);
            
            translate([0, 0, transition_length+lower_length])
            upper_part(upper_length);
        }
        if(for_3d_printer) {
            lower_length = bar_length - transition_length - upper_length;
            translate([0, 0, -epsilon])
            cylinder(h=lower_length+2*epsilon, r=bar_diameter/2 - wall);
            
            translate([0, 0, lower_length])
            transition(transition_length, bar_diameter-2*wall);
            
            translate([0, 0, transition_length+lower_length])
            upper_part(upper_length-wall, bar_diameter-wall*2);
        }
    }
}

module right_bar() {
    bar(right_bar_transition_length,
        right_bar_upper_length);
}

module left_bar() {
    bar(left_bar_transition_length,
        left_bar_upper_length);
}

translate([-(wall + bar_diameter)/2, 0, 0]) right_bar();
translate([(wall + bar_diameter)/2, 0, 0]) left_bar();
