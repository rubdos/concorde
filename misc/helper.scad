epsilon = 0.1;
module torus_part(R, r, angle=360, attached=false, hollow=false) {
    if(attached) {
        translate([-R, 0, 0])
        difference() {
            torus_part_impl(R, r, angle);
            if(hollow) {
                torus_part_impl(R, r-hollow, 360+epsilon);
            }
        }
    } else {
        difference() {
            torus_part_impl(R, r, angle);
            if(hollow) {
                torus_part_impl(R, r-hollow, 360+epsilon);
            }
        }
    }
}

module torus_part_impl(R, r, angle) {
    rotate_extrude(angle=angle, convexity=20)
    translate([R, 0, 0])
    circle(r=r);
}