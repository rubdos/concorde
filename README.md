# Concorde project

This project is my aim to build a home simulator compatible with the
[FlightGear][flightgear.org]
[Concorde][flightgear-concorde].

[flightgear.org]: http://www.flightgear.org/ "The main FlightGear website"
[flightgear-concorde]: http://wiki.flightgear.org/Concorde "FlightGear Concorde Wiki page"
[gplv2]: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
